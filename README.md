## barman
Manage your lemonbar with ease.

### Synopsis
This program is meant to make customizing lemonbar relatively easy, by providing pre-built "gadgets" that can be put together in a configuration file. Hopefully this program provides
enough functionality to where the user does not have to edit the source, but modifying the program to meet personal needs is encouraged.

This project is based on my early lemonbar project in my `dotfiles` repo, but written in C++ rather than Python to improve performance and get rid of some of the issues I dealt with.

### License
This program is licensed under the GNU GPL, version 2 or later.

