# Makefile
#
# barman - A program to manage your lemonbar
# Copyright (c) 2016 Ammon Smith
# 
# barman is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
# 
# barman is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with barman.  If not, see <http://www.gnu.org/licenses/>.
#

.PHONY: all force debug forcedebug test man clean

CC = g++
SRCDIR = src
BUILDDIR = build
EXE = build/barman
TESTER = test/tester

SRCEXT = cpp
OBJEXT = cpp.o
SOURCES = $(shell find $(SRCDIR) -type f -iname *.$(SRCEXT))
OBJECTS = $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.$(OBJEXT)))
FLAGS = -std=c++11 -pipe -Wall -Wextra -O3
INC = -I $(SRCDIR)
LIB = 

all: $(EXE)

$(EXE): $(OBJECTS)
	@mkdir -p '$(BUILDDIR)'
	@echo '[LN] $(shell basename $(EXE))'
	@$(CC) $(LIB) $(DEBUG) $^ -o $(EXE)

$(BUILDDIR)/%.$(OBJEXT): $(SRCDIR)/%.$(SRCEXT)
	@mkdir -p '$(BUILDDIR)'
	@echo '[CC] $(shell basename $@)'
	@$(CC) $(FLAGS) $(LIB) $(INC) $(DEBUG) -c -o $@ $<

$(TESTER):
	$(CC) $(FLAGS) $(LIB) $(INC) test/Tester.cpp -o  $(TESTER)

test: $(TESTER)
	@$(TESTER)

man:
	make -C man

clean:
	@echo '[RM] $(BUILDDIR)'
	@rm -rf '$(BUILDDIR)'
	make -C man clean

force:
	@echo '[RM] $(BUILDDIR)'
	@rm -rf '$(BUILDDIR)'
	make $(EXE)

debug:
	make $(EXE) DEBUG='-Og -g'

forcedebug: clean debug

