/* Constants.hpp
 *
 * barman - A program to manage your lemonbar
 * Copyright (c) 2016 Ammon Smith
 * 
 * barman is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * barman is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with barman.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __CONSTANTS_HPP
#define __CONSTANTS_HPP

#define DEBUG_MODE                 1

#define VERSION_STRING             "0.1"
#define ERROR_STRING               "<error>"

#define TICK_DELAY                 (1 / 6.0)

#define DEGREES                    "\xc2\xb0"
#define SEPARATOR_LEFT             "\xee\x82\xb2"
#define SEPARATOR_RIGHT            "\xee\x82\xb0"
#define LIGHT_SEPARATOR_LEFT       "\xee\x82\xb3"
#define LIGHT_SEPARATOR_RIGHT      "\xee\x82\xb1"

/* Terminus icons */
#define ICON_CDROM                 "\xc3\x80"
#define ICON_SOCIAL                "\xc8\x81"
#define ICON_WINDOW                "\xc3\x82"
#define ICON_MENU                  "\xc3\x83"
#define ICON_FACE                  "\xc3\x84"
#define ICON_CONNECTED             "\xc3\x85"
#define ICON_HOME                  "\xc3\x86"
#define ICON_ARCH_LINUX            "\xc3\x87"
#define ICON_MONITOR               "\xc3\x88"
#define ICON_VERTICAL_WORKSPACES   "\xc3\x89"
#define ICON_TWO_BOXES             "\xc3\x8a"
#define ICON_SQUARE                "\xc3\x8b"
#define ICON_HORIZONTAL_WORKSPACES "\xc3\x8c"
#define ICON_APPLICATIONS          "\xc3\x8d"
#define ICON_MUSIC                 "\xc3\x8e"
#define ICON_CRATE                 "\xc3\x8f"
#define ICON_DOWN_ARROW            "\xc3\x90"
#define ICON_UP_ARROW              "\xc3\x91"
#define ICON_CHAT_BUBBLE           "\xc3\x92"
#define ICON_MAIL                  "\xc3\x93"
#define ICON_VOLUME                "\xc3\x94"
#define ICON_CLOCK                 "\xc3\x95"
#define ICON_DOT                   "\xc3\x96"
#define ICON_MULTIPLICATION_SIGN   "\xc3\x97"
#define ICON_WIFI                  "\xc3\x98"
#define ICON_TRANSMISSION          "\xc3\x99"
#define ICON_LIGHT_SEPARATOR_RIGHT "\xc3\x9a"
#define ICON_SEPARATOR_LEFT        "\xc3\x9b"
#define ICON_LIGHT_SEPARATOR_LEFT  "\xc3\x9c"
#define ICON_SEPARATOR_RIGHT       "\xc3\x9d"
#define ICON_MICROCHIP             "\xc3\x9e"
#define ICON_TALL_BLOCK            "\xc3\x9f"

/* Powerline characters */
#define LIGHT_SEPARATOR_LEFT       "\xee\x82\xb3"
#define LIGHT_SEPARATOR_RIGHT      "\xee\x82\xb1"
#define SEPARATOR_LEFT             "\xee\x82\xb2"
#define SEPARATOR_RIGHT            "\xee\x82\xb0"

/* Lemonbar formatting */
#define LEMONBAR_ALIGN_LEFT        "%{l}"
#define LEMONBAR_ALIGN_CENTER      "%{c}"
#define LEMONBAR_ALIGN_RIGHT       "%{r}"

enum Alignment {
    Left,
    Center,
    Right,
};

enum DataUnit {
    Bytes,
    Kilobytes,
    Megabytes,
};

enum GadgetResource {
    None,
    i3,
    xprop,
};

#define JOURNAL_FILE               "barman.log"
#define MAX_LOGGER_LEVEL           logger::DEBUG
#define JOURNAL_BUFFER_LINES       10
#define JOURNAL_TIMESTAMP_FORMAT   "%a %d %b %Y %H:%M:%S %p"

#endif /* __CONSTANTS_HPP */

