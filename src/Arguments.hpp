/* Arguments.hpp
 *
 * barman - A program to manage your lemonbar
 * Copyright (c) 2016 Ammon Smith
 * 
 * barman is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * barman is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with barman.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __ARGUMENTS_HPP
#define __ARGUMENTS_HPP

#include <string>

namespace option {
    class Arguments {
        private:
            const char *configFile;
            bool foreground;
            bool killPrevious;

        public:
            Arguments(const int argc, const char *argv[]);
            const char *getConfigFile() const;
            bool runsInForeground() const;
            bool getKillPrevious() const;

        private:
            void parseFlags(const char *flags);
            void setForeground();
            void setNoKill();
            void setVerbose();
            void setQuiet();
    };
}

#endif /* __ARGUMENTS_HPP */

