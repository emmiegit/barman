/* SystemStats.cpp
 *
 * barman - A program to manage your lemonbar
 * Copyright (c) 2016 Ammon Smith
 * 
 * barman is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * barman is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with barman.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <ctime>
#include <iostream>
#include <string>

#include "Config.hpp"
#include "SystemStats.hpp"

std::string formatted_time(const char *format)
{
    char buf[512];
    if (format[0] == '\0') {
        return "";
    }

    int ret = strftime(buf, 512, format, NULL);
    if (ret) {
        return std::string(buf);
    } else {
        std::cerr << "Time format failed for \"" << format << "\" failed." << std::endl;
        return ERROR_STRING;
    }
}

