/* Arguments.cpp
 *
 * barman - A program to manage your lemonbar
 * Copyright (c) 2016 Ammon Smith
 * 
 * barman is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.  * 
 * barman is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with barman.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdlib>
#include <cstring>
#include <iostream>
#include <string>

#include <libgen.h>

#include "Arguments.hpp"
#include "Constants.hpp"
#include "Logger.hpp"
#include "Main.hpp"
#include "Util.hpp"

static inline bool stringEquals(const char *a, const char *b);
static void helpAndQuit();
static void usageAndQuit();
static void versionAndQuit();

namespace {
    char *programName;
}

namespace option {
    Arguments::Arguments(const int argc, const char *argv[]) : foreground(false), killPrevious(true)
    {
        /* Get name of program */
        const size_t argzLength = strlen(argv[0]) + 1;
        char argzCopy[argzLength];
        strncpy(argzCopy, argv[0], argzLength);
        programName = basename(argzCopy);

        journal(logger::DEBUG) << "Program name is \"" << programName << "\".";

        /* Check argument count */
        if (argc == 1) {
            usageAndQuit();
        }

        /* Check arguments */
        for (int i = 1; i < argc - 1; i++) {
            const char *arg = argv[i];
            if (arg[0] == '\0') {
                /* String is empty */
                continue;
            } else if (arg[0] != '-') {
                /* Parse single flag options */
                parseFlags(argv[i]);
            } else if (arg[0] == '-' && arg[1] == '-') {
                /* Parse --something options */
                if (stringEquals(arg, "--foreground")) {
                    setForeground();
                } else if (stringEquals(arg, "--nokill")) {
                    setNoKill();
                } else if (stringEquals(arg, "--verbose")) {
                    setVerbose();
                } else if (stringEquals(arg, "--quiet")) {
                    setQuiet();
                } else {
                    usageAndQuit();
                }
            } else {
                /* Starts with exactly one '-' */
                parseFlags(argv[i] + 1);
            }
        }

        /* Check last argument (should be the config file) */
        configFile = argv[argc - 1];
        journal(logger::DEBUG) << "Last argument is \"" << configFile << "\".";

        if (stringEquals(configFile, "--help")) {
            helpAndQuit();
        } else if (stringEquals(configFile, "--version")) {
            versionAndQuit();
        } else if (stringEquals(configFile, "-")) {
            configFile = "/dev/stdin";
        } else if (configFile[0] == '-') {
            usageAndQuit();
        }

        /* Determine options from arguments and the configuration file */
        std::string errString;
        if (!util::isFile(configFile, errString)) {
            std::cerr << "Cannot open specified config file \"" << configFile << "\": ";
            std::cerr << errString << "." << std::endl;
            program::cleanup(1);
        }
    }

    const char * Arguments::getConfigFile() const
    {
        return configFile;
    }

    bool Arguments::runsInForeground() const
    {
        return foreground;
    }

    bool Arguments::getKillPrevious() const
    {
        return killPrevious;
    }

    void Arguments::parseFlags(const char *flags)
    {
        for (int i = 0; flags[i]; i++) {
            switch (flags[i]) {
                case 'f':
                    setForeground();
                    break;
                case 'n':
                    setNoKill();
                    break;
                case 'v':
                    setVerbose();
                    break;
                case 'q':
                    setQuiet();
                    break;
                default:
                    usageAndQuit();
            }
        }
    }

    void Arguments::setForeground()
    {
        journal(logger::DEBUG) << "Setting \"foreground\" to true.";
        foreground = true;
    }

    void Arguments::setNoKill()
    {
        journal(logger::DEBUG) << "Setting \"killPrevious\" to false.";
        killPrevious = false;
    }

    void Arguments::setVerbose()
    {
        journal(logger::INFO) << "Enabling verbose debug logging.";
        logger::setLogLevel(logger::DEBUG);
    }

    void Arguments::setQuiet()
    {
        logger::setLogLevel(logger::ERROR);
        wipeJournal();
    }
} /* namespace option */

static inline bool stringEquals(const char *a, const char *b)
{
    return strcmp(a, b) == 0;
}

static void helpAndQuit()
{
    closeJournal();
    std::cout << "Barman v" << VERSION_STRING << "\n";
    std::cout << programName << " [-n] lemonbar-config-file\n";
    std::cout << programName << " [--help | --version]\n\n";
    std::cout << " -f, --foreground  Don't daemonize, run in the foreground.\n";
    std::cout << " -n, --nokill      Don't kill an existing barman process\n";
    std::cout << "                   if its pid file is detected.\n";
    std::cout << " -v, --verbose     Print debugging information.\n";
    std::cout << " -q, --quiet       Suppress non-error logging.\n";
    program::cleanup(0);
}

static void usageAndQuit()
{
    closeJournal();
    std::cout << "Barman v" << VERSION_STRING << "\n";
    std::cout << programName << " [-f] [-n] [-v | -q] lemonbar-config-file\n";
    std::cout << programName << " [--help | --version]\n";
    program::cleanup(1);
}

static void versionAndQuit()
{
    closeJournal();
    std::cout << "Barman v" << VERSION_STRING << "\n";
    std::cout << "barman - A program to manage your lemonbar\n";
    std::cout << "Copyright (c) 2016 Ammon Smith\n";
    std::cout << "\n";
    std::cout << "barman is free software: you can redistribute it and/or modify\n";
    std::cout << "it under the terms of the GNU General Public License as published by\n";
    std::cout << "the Free Software Foundation, either version 2 of the License, or\n";
    std::cout << "(at your option) any later version.\n";
    std::cout << "\n";
    std::cout << "barman is distributed in the hope that it will be useful,\n";
    std::cout << "but WITHOUT ANY WARRANTY; without even the implied warranty of\n";
    std::cout << "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n";
    std::cout << "GNU General Public License for more details.\n";
    std::cout << "\n";
    std::cout << "You should have received a copy of the GNU General Public License\n";
    std::cout << "along with barman.  If not, see <http://www.gnu.org/licenses/>.\n";
    std::cout << "\n";
    program::cleanup(0);
}


