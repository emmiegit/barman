/* Config.cpp
 *
 * barman - A program to manage your lemonbar
 * Copyright (c) 2016 Ammon Smith
 * 
 * barman is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * barman is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with barman.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cerrno>
#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>

#include <libgen.h>

#include "Config.hpp"
#include "Logger.hpp"
#include "Main.hpp"
#include "Util.hpp"

static void findLine(const char *path, size_t &lineno, std::string &line, const std::string &pattern);
static void printError(const char *path, size_t lineno, const std::string &line, const std::string &errorMessage);
static option::Target getTarget(const std::string &str);
static templ::GadgetType getGadgetType(const std::string &str);
static const std::string getGadgetName(templ::GadgetType type);

namespace option {
    Configuration::Configuration(const char *path)
    {
        std::ifstream fh(path);
        if (!fh.is_open()) {
            journal(logger::ERROR) << "Unable to open configuration file: \"" << path << "\": " \
                << strerror(errno) << "." << std::endl;
            program::cleanup(1);
        }

        journal(logger::DEBUG) << "Reading configuration file: \"" << path << "\".";

        Target target = Invalid;
        templ::GadgetTemplate gadget;
        gadget.type = templ::Invalid;

        std::string line;
        for (size_t lineno = 1; std::getline(fh, line); lineno++) {
            util::trim(line);

            if (line.empty() || line.at(0) == '#') {
                /* Ignore comments and blank lines */
                continue;
            } else if (line.at(0) == '.') {
                /* It's an element */
                if (gadget.type == templ::Invalid) {
                    printError(path, lineno, line, "Cannot set gadget without a corresponding target");
                }

                const size_t index = line.find(' ');
                if (index == std::string::npos) {
                    printError(path, lineno, line, "No space between key and value");
                }

                std::string key = line.substr(1, index - 1);
                std::string value = line.substr(index + 1);

                journal(logger::DEBUG) << "Element for " << getGadgetName(gadget.type) << ": \"" << key << "\" = \"" << value << "\".";

                gadget.options.insert(std::make_pair(key, value));
            } else if (line.at(line.size() - 1) == ':') {
                /* If there's a previous gadget, push it */
                if (gadget.type != templ::Invalid) {
                    journal(logger::DEBUG) << "Adding " << getGadgetName(gadget.type) << " to gadget vector.";
                    gadgets.push_back(gadget);
                    gadget.type = templ::Invalid;
                    gadget.options.clear();
                }

                /* It's a target */
                journal(logger::DEBUG) << "Got target: \"" << line << "\".";

                target = getTarget(line);

                if (target == Invalid) {
                    printError(path, lineno, line, "Invalid target");
                }
            } else {
                /* It's either a "key = value" or gadget */
                const size_t index = line.find('=');

                if (index == std::string::npos) {
                    /* If there's a previous gadget, push it */
                    if (gadget.type != templ::Invalid) {
                        journal(logger::DEBUG) << "Adding " << getGadgetName(gadget.type) << " to gadget vector.";
                        gadgets.push_back(gadget);
                        gadget.type = templ::Invalid;
                        gadget.options.clear();
                    }

                    /* Parse it like it's a gadget */
                    journal(logger::DEBUG) << "Got gadget: \"" << line << "\".";

                    if (target == Invalid) {
                        printError(path, lineno, line, "Cannot set gadget without a corresponding target");
                    }

                    gadget.type = getGadgetType(line);

                    if (gadget.type == templ::Invalid) {
                        printError(path, lineno, line, "Invalid gadget identifier");
                    }
                } else {
                    /* It's a "key = value" setting */
                    std::string key = line.substr(0, index - 1);
                    std::string value = line.substr(index + 1);
                    util::trim(key);
                    util::trim(value);

                    journal(logger::INFO) << "Setting \"" << key << "\" to \"" << value << "\".";
                    options.insert(std::make_pair(key, value));
                }
            }
        }

        /* Evaluate option values. */
        for (std::map<std::string, std::string>::iterator iter = options.begin();
                iter != options.end();
                ++iter) {
            if (!iter->second.empty() && iter->second.at(0) == '$') {
                std::string varname = iter->second.substr(1);

                std::map<std::string, std::string>::const_iterator item = options.find(varname);

                if (item != options.end()) {
                    journal(logger::DEBUG) << "Setting \"" << iter->first << "\" to \"" << iter->second \
                        << "\": \"" << item->second << "\".";
                    options[iter->first] = item->second;
                } else { 
                    size_t lineno;
                    std::string line;
                    findLine(path, lineno, line, iter->second);
                    printError(path, lineno, line, "Unknown variable referenced: " + iter->second);
                }
            }
        }
    }

    const std::string Configuration::getOption(const std::string &key) const
    {
        return options.at(key);
    }

    bool Configuration::hasOption(const std::string &key) const
    {
        return options.find(key) != options.end();
    }

    void Configuration::printConfig() const
    {
        journal(logger::DEBUG) << "Configuration settings:";
        for (std::map<std::string, std::string>::const_iterator iter = options.begin();
                iter != options.end();
                ++iter) {
            journal(logger::DEBUG) << '\t' << iter->first << " : " << iter->second;
        }

        journal(logger::DEBUG) << "Configuration object dump:";
        for (std::vector<templ::GadgetTemplate>::const_iterator iter = gadgets.begin();
                iter != gadgets.end();
                ++iter) {
            printGadgetTemplate(*iter);
        }

        journal(logger::DEBUG) << "Configuration object dump finished.";
    }

    void Configuration::printGadgetTemplate(const templ::GadgetTemplate &gtemplate)
    {
        journal(logger::DEBUG) << "Gadget type: " << getGadgetName(gtemplate.type);
        journal(logger::DEBUG) << "Properties:";

        std::map<const std::string, const std::string> options;
        for (std::map<const std::string, const std::string>::const_iterator iter = gtemplate.options.begin();
                iter != gtemplate.options.end();
                ++iter) {
            journal(logger::DEBUG) << '\t' << iter->first << " : " << iter->second;
        }
    }
} /* namespace config */

static void findLine(const char *path, size_t &lineno, std::string &line, const std::string &pattern)
{
        std::ifstream fh(path);
        if (!fh.is_open()) {
            journal(logger::ERROR) << "Unable to reopen configuration file for error reporting: \"" \
                 << path << "\": " << strerror(errno) << ".";
            program::cleanup(2);
        }

        for (lineno = 1; std::getline(fh, line); lineno++) {
            if (line.find(pattern) != std::string::npos) {
                return;
            }
        }

        journal(logger::ERROR) << "Couldn't find matching error matching line in configuration file.";
        program::cleanup(2);
}

static void printError(const char *path, size_t lineno, const std::string &line, const std::string &errorMessage)
{
    journal(logger::ERROR) << "Error while parsing configuration file, quitting.";
    std::cerr << basename((char *)path) << ":" << lineno << " ";
    std::cerr << errorMessage << ": " << line << "." << std::endl;
    program::cleanup(1);
}

static option::Target getTarget(const std::string &str)
{
    if (str == "left:")                return option::Left;
    if (str == "center:")              return option::Center;
    if (str == "right:")               return option::Right;
    if (str == "nextmonitor:")         return option::NextMonitor;
    if (str == "previousmonitor:")     return option::PreviousMonitor;
    if (str == "firstmonitor:")        return option::FirstMonitor;
    if (str == "lastmonitor:")         return option::LastMonitor;
    if (str == "monitor0:")            return option::Monitor0;
    if (str == "monitor1:")            return option::Monitor1;
    if (str == "monitor2:")            return option::Monitor2;
    if (str == "monitor3:")            return option::Monitor3;
    if (str == "monitor4:")            return option::Monitor4;
    if (str == "monitor5:")            return option::Monitor5;
    if (str == "monitor6:")            return option::Monitor6;
    if (str == "monitor7:")            return option::Monitor7;
    if (str == "monitor8:")            return option::Monitor8;
    if (str == "monitor9:")            return option::Monitor9;
    else                               return option::Invalid;
}

static templ::GadgetType getGadgetType(const std::string &str)
{
    if (str == "Workspaces")           return templ::Workspaces;
    if (str == "WindowTitle")          return templ::WindowTitle;
    if (str == "NowPlaying")           return templ::NowPlaying;
    if (str == "Volume")               return templ::Volume;
    if (str == "CPU")                  return templ::CPU;
    if (str == "Memory")               return templ::Memory;
    if (str == "Network")              return templ::Network;
    if (str == "Time")                 return templ::Time;
    if (str == "Battery")              return templ::Battery;
    if (str == "WeatherFahrenheit")    return templ::WeatherFahrenheit;
    if (str == "WeatherCelsius")       return templ::WeatherCelsius;
    if (str == "Space")                return templ::Space;
    if (str == "StaticText")           return templ::StaticText;
    else                               return templ::Invalid;
}

static const std::string getGadgetName(templ::GadgetType type)
{
    switch (type) {
        case templ::Workspaces:        return "Workspaces";
        case templ::WindowTitle:       return "WindowTitle";
        case templ::NowPlaying:        return "NowPlaying";
        case templ::Volume:            return "Volume";
        case templ::CPU:               return "CPU";
        case templ::Memory:            return "Memory";
        case templ::Network:           return "Network";
        case templ::Time:              return "Time";
        case templ::Battery:           return "Battery";
        case templ::WeatherFahrenheit: return "WeatherFahrenheit";
        case templ::WeatherCelsius:    return "WeatherCelsius";
        case templ::Space:             return "Space";
        case templ::StaticText:        return "StaticText";
        default:                       return ERROR_STRING;
    }
}

