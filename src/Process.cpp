/* Process.cpp
 *
 * barman - A program to manage your lemonbar
 * Copyright (c) 2016 Ammon Smith
 *
 * barman is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * barman is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with barman.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cerrno>
#include <cstddef>
#include <cstdio>
#include <cstring>
#include <string>

#include <unistd.h>
#include <sys/types.h>

#include "Config.hpp"
#include "Logger.hpp"
#include "Main.hpp"
#include "Process.hpp"

/* Static declarations */
static char** buildLemonbarArguments(const option::Configuration &config);

/* Static variables */
namespace {
    FILE *barHandle;
}

/* Function implementations */
namespace process {
    void init(const option::Configuration &config)
    {
        char** barArguments = buildLemonbarArguments(config);

        /* Create lemonbar pipe */
        int barDescriptors[2];
        pid_t barpid;
        
        pipe(barDescriptors);
        barpid = fork();

        if (barpid == -1) {
            const int errno_ = errno;
            journal(logger::ERROR) << "Unable to create lemonbar child process: " << strerror(errno_) << ".";
            program::cleanup(1);
        } else if (barpid == 0) {
            /* Child process */
            close(0);
            dup(barDescriptors[0]);

            // TODO manipulate stdout to be a pipe

            const char *programName = "/home/ammon/Programming/Misc/c/bin/arg-echo";
            /*
            if (config.hasOption("bar_program")) {
                programName = config.getOption("bar_program").c_str();
            } else {
                programName = "lemonbar";
            }
            */

            int ret = execvp(programName, barArguments);
            if (ret < 0) {
                const int errno_ = errno;
                journal(logger::ERROR) << "Unable to execute lemonbar process: " << strerror(errno_) << ".";
                program::cleanup(1);
            }
        } else {
            /* Parent process */
            barHandle = fdopen(barDescriptors[1], "w");
            fclose(barHandle);
        }

        /*
         bar_proc = popen(command, stdin=PIPE, stdout=PIPE);
         sh_proc  = popen("sh", stdin=bar_proc.stdout, stdout=DEVNULL);
         */
    }
}; /* namespace process */

static char** buildLemonbarArguments(const option::Configuration &config)
{
    std::vector<const char *> arguments;
    journal(logger::DEBUG) << "Generating lemonbar arguments.";

    if (config.hasOption("background_color")) {
        arguments.push_back("-B");
        arguments.push_back(config.getOption("background_color").c_str());
    }

    if (config.hasOption("foreground_color")) {
        arguments.push_back("-F");
        arguments.push_back(config.getOption("foreground_color").c_str());
    }

    if (config.hasOption("max_clickable_areas")) {
        arguments.push_back("-a");
        arguments.push_back(config.getOption("max_clickable_areas").c_str());
    }

    if (config.hasOption("window_name")) {
        arguments.push_back("-n");
        arguments.push_back(config.getOption("window_name").c_str());
    }

    if (config.hasOption("offset")) {
        arguments.push_back("-o");
        arguments.push_back(config.getOption("offset").c_str());
    }

    if (config.hasOption("underline_width")) {
        arguments.push_back("-u");
        arguments.push_back(config.getOption("underline_width").c_str());
    }

    if (config.hasOption("dock_at_bottom")) {
        arguments.push_back("-b");
    }

    if (config.hasOption("force_docking")) {
        arguments.push_back("-d");
    }

    /* Geometry option (-g) */
    const bool hasBarWidth = config.hasOption("bar_width");
    const bool hasBarHeight = config.hasOption("bar_height");
    const bool hasBarX = config.hasOption("bar_x_pos");
    const bool hasBarY = config.hasOption("bar_y_pos");

    if (hasBarWidth || hasBarHeight || hasBarX || hasBarY) {
        std::string geometry;

        if (hasBarWidth) {
            geometry.append(config.getOption("bar_width"));
        }

        geometry.push_back('x');

        if (hasBarHeight) {
            geometry.append(config.getOption("bar_height"));
        }

        geometry.push_back('+');

        if (hasBarX) {
            geometry.append(config.getOption("bar_x_pos"));
        }

        geometry.push_back('+');

        if (hasBarY) {
            geometry.append(config.getOption("bar_y_pos"));
        }

        char *cstr = new char[geometry.size()];
        strncpy(cstr, geometry.c_str(), geometry.size());

        arguments.push_back("-g");
        arguments.push_back(cstr);
    }
    
    if (config.hasOption("fonts")) {
        std::stringstream strm(config.getOption("fonts"));
        std::string font;

        while (std::getline(strm, font, ';')) {
            char *cstr = new char[font.size()];
            strncpy(cstr, font.c_str(), font.size());

            arguments.push_back("-f");
            arguments.push_back(cstr);
        }
    }

    char **command = new char *[arguments.size() + 1];

    for (size_t i = 0; i < arguments.size(); i++) {
        command[i] = (char *)arguments.at(i);
    }

    command[arguments.size()] = NULL;

#if DEBUG_MODE
    journal(logger::DEBUG) << "Built lemonbar arguments:";
    for (std::vector<const char *>::const_iterator iter = arguments.begin();
            iter != arguments.end();
            ++iter) {
        journal(logger::DEBUG) << "  " << *iter;
    }
#endif /* DEBUG_MODE */

    return command;
}

