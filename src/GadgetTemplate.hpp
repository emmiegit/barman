/* GadgetTemplate.hpp
 *
 * barman - A program to manage your lemonbar
 * Copyright (c) 2016 Ammon Smith
 * 
 * barman is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * barman is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with barman.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GADGETTEMPLATE_HPP
#define __GADGETTEMPLATE_HPP

#include <map>
#include <string>

#include "Gadget.hpp"

namespace templ {
    enum GadgetType {
        /* i3 */
        Workspaces,

        /* Window Manager */
        WindowTitle,

        /* Media */
        NowPlaying,
        Volume,

        /* System */
        CPU,
        Memory,
        Network,
        Time,
        Battery,

        /* Weather */
        WeatherFahrenheit,
        WeatherCelsius,

        /* Misc */
        Space,
        StaticText,

        /* Others */
        Invalid,
    };

    struct GadgetTemplate {
        GadgetType type;
        std::map<const std::string, const std::string> options;
    };
}

#endif /* __GADGETTEMPLATE_HPP */

