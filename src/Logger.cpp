/* Logger.cpp

* dad's email
* hope we will be able to talk like we used to
* the philosophies hat govern the way i think have been very valuable to my development
* however, one of my axioms is different

 *
 * barman - A program to manage your lemonbar
 * Copyright (c) 2016 Ammon Smith
 * 
 * barman is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * barman is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with barman.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>

#include <stdio.h>
#include <time.h>

#include "Constants.hpp"
#include "Logger.hpp"

#define TIMESTAMP_BUFFER_SIZE 80

/* Static variables */
namespace {
    const char * const LOGGER_LEVEL_NAMES[] = {
        " [ERROR] ",
        " [WARN]  ",
        " [INFO]  ",
        " [DEBUG] ",
    };

    /*
    const char * const LOGGER_LEVEL_COLOR_NAMES[] = {
        " [\x1b[31m\x1b[1mERROR\x1b[0m] ",
        " [\x1b[33mWARN\x1b[0m]  ",
        " [INFO]  ",
        " [\x1b[34mDEBUG\x1b[0m] ",
    };
    */

#if DEBUG_MODE
    logger::LogLevel level = logger::DEBUG;
#else
    logger::LogLevel level = logger::INFO;
#endif /* DEBUG_MODE */

    logger::Logger log;
    std::ostringstream nullStream;
}

/* Function implementations */
namespace logger {
    Logger::Logger(const char *journalFile) : open(true), lines(0)
    {
        ostream = stdout;
        fstream = fopen(journalFile, "a");
    }

    Logger::~Logger()
    {
        if (buffer.tellp() > 0) {
            buffer << "\n";
        }

        flush();

        if (ostream) {
            fclose(ostream);
        }

        if (fstream) {
            fclose(fstream);
        }
    }

    std::ostringstream &Logger::getStream(LogLevel level)
    {
        if (!open) {
            return nullStream;
        }

        if ((lines++) >= JOURNAL_BUFFER_LINES) {
            flush();
            lines = 0;
        } else if (buffer.tellp() > 0) {
            buffer << "\n";
        }

        /* Create timestamp */
        char timestamp[TIMESTAMP_BUFFER_SIZE];
        time_t now = time(0);
        size_t ret = strftime(timestamp, TIMESTAMP_BUFFER_SIZE, JOURNAL_TIMESTAMP_FORMAT, localtime(&now));

        if (ret) {
            buffer << timestamp;
        } else {
            buffer << ERROR_STRING;
        }

        buffer << LOGGER_LEVEL_NAMES[level];
        return buffer;
    }

    LogLevel & Logger::reportingLevel()
    {
        return level;
    }

    bool Logger::canWrite() const
    {
        return (bool)(ostream || fstream);
    }

    void Logger::wipe()
    {
        buffer.str("");
    }

    void Logger::flush()
    {
        if (buffer.tellp() == 0 || !open) {
            return;
        }

        std::string bufferContents = buffer.str();
        buffer.str("");
        nullStream.str("");

        if (bufferContents.at(bufferContents.length() - 1) != '\n') {
            bufferContents.push_back('\n');
        }

        if (ostream) {
            fprintf(ostream, "%s", bufferContents.c_str());
        }

        if (fstream) {
            fprintf(fstream, "%s", bufferContents.c_str());
        }
    }

    void Logger::close()
    {
        open = false;
    }

    void setLogLevel(LogLevel newLevel)
    {
        level = newLevel;
    }
} /* namespace logger */

std::ostringstream &journal(logger::LogLevel level)
{
    if (level > MAX_LOGGER_LEVEL || level > log.reportingLevel() || !log.canWrite()) {
        return nullStream;
    } else {
        return log.getStream(level);
    }
}

void wipeJournal()
{
    log.wipe();
}

void flushJournal()
{
    log.flush();
}

void closeJournal()
{
    log.close();
}

