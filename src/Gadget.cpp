/* Gadget.cpp
 *
 * barman - A program to manage your lemonbar
 * Copyright (c) 2016 Ammon Smith
 * 
 * barman is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * barman is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with barman.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Constants.hpp"
#include "Gadget.hpp"

namespace gadget {

Gadget::Gadget(const Alignment alignment, const GadgetResource wants) :
    alignment(alignment), wants(wants)
{
    // TODO
}

Gadget::~Gadget()
{
    // TODO
}

} /* namespace gadget */

