/* Util.cpp
 *
 * barman - A program to manage your lemonbar
 * Copyright (c) 2016 Ammon Smith
 * 
 * barman is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * barman is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with barman.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>

#ifndef __UTIL_HPP
#define __UTIL_HPP

namespace util {
    bool isFile(const char *path, std::string &errString);
    bool isDirectory(const char *path, std::string &errString);

    inline void ltrim(std::string &str);
    inline void rtrim(std::string &str);
    void trim(std::string &str);
}

#endif /* __UTIL_HPP */

