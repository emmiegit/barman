/* Logger.hpp
 *
 * barman - A program to manage your lemonbar
 * Copyright (c) 2016 Ammon Smith
 * 
 * barman is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * barman is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with barman.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __LOGGER_HPP
#define __LOGGER_HPP

#include <cstdio>
#include <iostream>
#include <sstream>
#include <string>

#include "Constants.hpp"

namespace logger {
    enum LogLevel {
        ERROR,
        WARNING,
        INFO,
        DEBUG,
    };

    class Logger {
        private:
            std::ostringstream buffer;
            FILE *ostream;
            FILE *fstream;
            bool open;
            unsigned int lines;

        public:
            Logger(const char *journalFile = JOURNAL_FILE);
            virtual ~Logger();
            std::ostringstream &getStream(LogLevel level);
            bool canWrite() const;
            void wipe();
            void flush();
            void close();
            Logger(const Logger &orig);
            Logger & operator=(const Logger &orig);

        public:
            static LogLevel & reportingLevel();

        private:
            int bufferLength() const;
    };

    void setLogLevel(LogLevel newLevel);
} /* namespace logger */

std::ostringstream &journal(logger::LogLevel level);
void setLogLevel(logger::LogLevel level);
void wipeJournal();
void flushJournal();
void closeJournal();

#endif /* __LOGGER_HPP */

