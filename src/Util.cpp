/* Util.hpp
 *
 * barman - A program to manage your lemonbar
 * Copyright (c) 2016 Ammon Smith
 * 
 * barman is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * barman is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with barman.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <algorithm>
#include <cctype>
#include <cerrno>
#include <cstring>
#include <functional>
#include <locale>
#include <string>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "Util.hpp"

namespace util {

bool isFile(const char *path, std::string &errString)
{
    struct stat buf;
    int ret = stat(path, &buf);

    if (ret < 0) {
        errString = std::string(strerror(errno));
        return false;
    } else if (!S_ISDIR(buf.st_mode)) {
        return true;
    } else {
        errString = "Not a file";
        return false;
    }
}

bool isDirectory(const char *path, std::string &errString)
{
    struct stat buf;
    int ret = stat(path, &buf);

    if (ret < 0) {
        errString = std::string(strerror(errno));
        return false;
    } else if (S_ISDIR(buf.st_mode)) {
        return true;
    } else {
        errString = "Not a directory";
        return false;
    }
}

/* Trim whitespace in place from start */
inline void ltrim(std::string &str)
{
    str.erase(str.begin(), std::find_if(
                str.begin(), str.end(), std::not1(
                std::ptr_fun<int, int>(std::isspace))));
}

/* Trim whitespace in place from end */
inline void rtrim(std::string &str)
{
    str.erase(std::find_if(str.rbegin(), str.rend(),
                std::not1(std::ptr_fun<int, int>(std::isspace)))
                .base(), str.end());
}

/* Trim whitespace in place */
void trim(std::string &str)
{
    ltrim(str);
    rtrim(str);
}

} /* namespace util */

