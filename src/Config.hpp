/* Config.hpp
 *
 * barman - A program to manage your lemonbar
 * Copyright (c) 2016 Ammon Smith
 * 
 * barman is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * barman is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with barman.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __CONFIG_HPP
#define __CONFIG_HPP

#include <vector>

#include "GadgetTemplate.hpp"

namespace option {
    /* Object to store configuration settings */
    class Configuration {
        private:
            std::map<std::string, std::string> options;
            std::vector<templ::GadgetTemplate> gadgets;
        
        public:
            Configuration(const char *path);
            const std::string getOption(const std::string &key) const;
            bool hasOption(const std::string &key) const;

            /* Debug methods */
            void printConfig() const;
            static void printGadgetTemplate(const templ::GadgetTemplate &gtemplate);
    };

    /* Types for parsing config file */
    enum Target {
        Left,
        Center,
        Right,
        NextMonitor,
        PreviousMonitor,
        FirstMonitor,
        LastMonitor,
        Monitor0,
        Monitor1,
        Monitor2,
        Monitor3,
        Monitor4,
        Monitor5,
        Monitor6,
        Monitor7,
        Monitor8,
        Monitor9,
        Invalid,
    };
} /* namespace option */

#endif /* __CONFIG_HPP */

