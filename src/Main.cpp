/* Main.cpp
 *
 * barman - A program to manage your lemonbar
 * Copyright (c) 2016 Ammon Smith
 * 
 * barman is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * barman is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with barman.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cerrno>
#include <csignal>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <iostream>

#include <limits.h>
#include <unistd.h>

#include "Arguments.hpp"
#include "Config.hpp"
#include "Constants.hpp"
#include "GadgetController.hpp"
#include "Logger.hpp"
#include "Main.hpp"
#include "Process.hpp"
#include "SignalHandler.hpp"
#include "Util.hpp"

#define __MAIN_CPP

/* Static declarations */
static void daemonize();
static pid_t checkPidFile(const char *pidFile);
static void createPidFile(bool killPrevious);
static void deletePidFile();

/* Static variables */
namespace {
    bool madePidFile = false;
    option::Arguments *args;
    option::Configuration *config;
    // gadget::GadgetController *controller;
}

/* Function implementations */
int main(const int argc, const char *argv[])
{
    signals::init();
    args = new option::Arguments(argc, argv);
    daemonize();
    createPidFile(args->getKillPrevious());
    config = new option::Configuration(args->getConfigFile());
    process::init(*config);

    flushJournal();
    program::cleanup(0);
}

namespace program {
    void cleanup(const int ret)
    {
        if (madePidFile) {
            deletePidFile();
        }

        delete args;
        delete config;
        journal(logger::INFO) << "Exiting with return code " << ret << ".";
        exit(ret);
    }

    void reloadConfig()
    {
        delete config;
        config = new option::Configuration(args->getConfigFile());
    }
} /* namespace program */

/* Static function implementations */
static void daemonize()
{
    if (args->runsInForeground()) {
        return;
    }

    /* Fork to background */
    pid_t pid;
    if ((pid = fork()) < 0) {
        program::cleanup(1);
        return;
    } else if (pid != 0) {
        /* We're the parent, so quit */
        journal(logger::INFO) << "Successfully created background daemon, pid " << pid << ".";
        program::cleanup(0);
        return;
    }

    /* We're the child, so daemonize */
    pid = setsid();

    if (pid < 0) {
        const int errno_ = errno;
        journal(logger::ERROR) << "Unable to create new session: " << strerror(errno_);
        return;
    } else {
        journal(logger::INFO) << "Successfully detached from parent. Now running as a daemon.";
    }
}

static pid_t checkPidFile(const char *pidFile)
{
    std::ifstream fh;
    int pid;
    fh.open(pidFile);

    if (fh.is_open()) {
        fh >> pid;
        return static_cast<pid_t>(pid);
    } else {
        std::cerr << "Unable to open existing pid file at \"" << pidFile << "\": ";
        std::cerr << strerror(errno) << "." << std::endl;
        return static_cast<pid_t>(0);
    }
}

static const char *getPidFile()
{
    char *pidFile = new char[PATH_MAX];
    std::string errString;

    if (util::isDirectory("/run", errString)) {
        snprintf(pidFile, PATH_MAX, "/run/user/%d/barman.pid", getuid());
    } else {
        snprintf(pidFile, PATH_MAX, "/var/run/user/%d/barman.pid", getuid());
    }

    return pidFile;
}

static void createPidFile(bool killPrevious)
{
    const char *pidFile = getPidFile();
    std::string errString;
    if (util::isFile(pidFile, errString)) {
        const pid_t pid = checkPidFile(pidFile);
        journal(logger::INFO) << "Pidfile exists: \"" << pidFile << "\".";

        if (pid) {
            if (killPrevious) {
                int ret = kill(pid, SIGTERM);

                if (ret < 0) {
                    if (errno == ESRCH) {
                        /* No such process, remove pidfile */
                        deletePidFile();
                    } else {
                        std::cerr << "Unable to kill previous instance: ";
                        std::cerr << strerror(errno) << "." << std::endl;
                        program::cleanup(1);
                    }
                }
            } else {
                std::cerr << "Barman instance already running (pid " << pid << ")\n";
                std::cerr << "To run anyways, killing the previous instance, omit the -n flag." << std::endl;
                program::cleanup(1);
            }
        } else {
            std::cerr << "Corrupt pid file at \"" << pidFile << "\", running anyways." << std::endl;
        }
    } else {
        journal(logger::INFO) << "Pidfile does not exist: \"" << pidFile << "\".";
    }

    std::ofstream fh;
    fh.open(pidFile);
    if (fh.is_open()) {
        journal(logger::INFO) << "Writing \"" << getpid() << "\" to pidfile.";
        fh << getpid() << "\n";
        fh.close();
    } else {
        std::cerr << "Unable to open pid file at \"" << pidFile << "\": " << strerror(errno);
        std::cerr << "." << std::endl;
        program::cleanup(1);
    }
    
    madePidFile = true;
    delete pidFile;
}

static void deletePidFile()
{
    const char *pidFile = getPidFile();
    std::string errString;

    if (util::isFile(pidFile, errString)) {
        const pid_t pid = checkPidFile(pidFile);

        if (pid != getpid()) {
            journal(logger::ERROR) << "Pidfile refers to process " << pid << ", but we are process " << getpid() << "!\n";
            journal(logger::ERROR) << "Not removing pidfile." << std::endl;
        } else {
            int ret = unlink(pidFile);
            journal(logger::INFO) << "Removing pidfile: \"" << pidFile << "\".";

            if (ret < 0) {
                journal(logger::ERROR) << "Unable to remove pidfile: " << strerror(errno) << "." << std::endl;
            }
        }
    } else {
        journal(logger::WARNING) << "Can't delete pidfile because it disappeared.";
    }

    delete pidFile;
}

